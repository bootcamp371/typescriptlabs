let scores: number[] = [75,80,85,90,95,100,71];
let totalScore: number = 0;

for (let i = 0; i < scores.length; i++){

    totalScore = totalScore + scores[i];
}

console.log("The average score was " + totalScore/scores.length);
scores.sort(compareFn);

console.log("The high score was " + scores[6]);
console.log("The low score was " + scores[0]);

function compareFn(a:number , b:number) {
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }
    // a must be equal to b
    return 0;
  }