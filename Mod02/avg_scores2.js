"use strict";
let scores2 = [
    93, 86, 73, 79, 83, 100, 94
];
let sum = sumNumbersInArray(scores2);
let average = sum / scores2.length;
;
scores2.sort(compareScores);
let highScore = getHighScore(scores2);
let lowScore = getLowScore(scores2);
// The displayStat function will concatenate the two
// values passed separated by a color and display
// the result
displayStat("Average", average);
displayStat("High Score", highScore);
displayStat("Low Score", lowScore);
function sumNumbersInArray(scores2) {
    let myTotal = 0;
    for (let i = 0; i < scores2.length; i++) {
        myTotal = myTotal + scores2[i];
    }
    return myTotal;
}
function getHighScore(scores2) {
    return scores2[6];
}
function getLowScore(scores2) {
    return scores2[0];
}
function compareScores(a, b) {
    if (a < b) {
        return -1;
    }
    if (a > b) {
        return 1;
    }
    // a must be equal to b
    return 0;
}
function displayStat(type, scoreValue) {
    console.log(type + " " + scoreValue);
}
