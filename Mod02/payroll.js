"use strict";
let payRate = 10.00;
let hoursWorked = 45;
let filingStatus = "Single";
let weeklyGrossPay = 0;
let yearlyGrossPay = 0;
let taxRate = 0;
if (hoursWorked <= 40) {
    weeklyGrossPay = hoursWorked * payRate;
}
else {
    weeklyGrossPay = (40 * payRate) +
        ((hoursWorked - 40) * (payRate * 1.5));
}
yearlyGrossPay = weeklyGrossPay * 52;
if (filingStatus = "Single") {
    if (yearlyGrossPay < 23000) {
        taxRate = .05;
    }
    if (yearlyGrossPay >= 23000 && yearlyGrossPay < 75000) {
        taxRate = .12;
    }
    if (yearlyGrossPay >= 75000) {
        taxRate = .20;
    }
}
if (filingStatus = "joint") {
    if (yearlyGrossPay < 23000) {
        taxRate = .00;
    }
    if (yearlyGrossPay >= 23000 && yearlyGrossPay < 75000) {
        taxRate = .09;
    }
    if (yearlyGrossPay >= 75000) {
        taxRate = .20;
    }
}
console.log("You worked " + hoursWorked + " this period");
console.log("Because you earn $" + payRate +
    " per hour, your gross pay is $" + weeklyGrossPay);
console.log("Your filing status is " + filingStatus);
console.log("Your tax withholdings this period is $" + (weeklyGrossPay * taxRate));
console.log("Your net pay is $" + (weeklyGrossPay - (weeklyGrossPay * taxRate)));
