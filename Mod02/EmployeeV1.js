"use strict";
class Employee {
    constructor(id, employeeName, jobTitle, payRate) {
        this.id = id;
        this.employeeName = employeeName;
        this.jobTitle = jobTitle;
        this.payRate = payRate;
    }
    toString() {
        return `${this.employeeName} ( ${this.id} ) is a ${this.jobTitle} earning $${this.payRate}/hr`;
    }
}
function toStringOut(emp) {
    return `${emp.employeeName} is a ${emp.jobTitle} earning $${emp.payRate}/hr`;
}
let emp1 = new Employee(1, "Mark", "Software Engineer", 40);
let emp2 = new Employee(2, "Sandy", "Teacher", 30);
console.log(emp1.toString());
console.log(emp2.toString());
console.log(toStringOut(emp1));
