let products: string[] = ["HAM-L-1220", "TURKEY-M-1125", "BEEF-S-1215"];

let firstDash: number = 0;
let secondDash: number = 0;
let meat: string = "";
let size: string = "";
let delDate: string = "";

for (let i = 0; i < products.length; i++) {

    firstDash = products[i].indexOf("-");
    secondDash = products[i].lastIndexOf("-");

    meat = products[i].substring(0, firstDash);
    size = products[i].substring(firstDash + 1, secondDash);
    delDate = products[i].substring(secondDash + 1);

    size = getSize(size);

    console.log(size + " " + meat + " was picked up on " +
        delDate.substring(0, 2) + "-" + delDate.substring(2));
}

function getSize(size: string): string {

    if (size == "L") {
        size = "Large";
    }
    else if (size == "M") {
        size = "Medium";
    } else {
        size = "Small";
    }
    return size;
}
